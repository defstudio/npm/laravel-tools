let tools = {};

import axios from 'axios';
axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
let token = document.head.querySelector('meta[name="csrf-token"]');
if(token){
	axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;
}else{
	console.error('CSRF token not found: https://laravel.com/docs/csrf#csrf-x-csrf-token');
}
window.spin_count = 0;
axios.interceptors.request.use(function(config){
	// noinspection JSUnresolvedVariable
	if(config.spinner ?? true){
		window.spin_count++;
		if(window.spin_count === 1){
			document.body.classList.add('axios-spinner');
		}
	}
	return config;
});
axios.interceptors.response.use(function(response){
	// noinspection JSUnresolvedVariable
	if(response.config.spinner ?? true){
		window.spin_count--;
		if(window.spin_count <= 0){
			window.spin_count = 0;
			document.body.classList.remove('axios-spinner');
		}
	}

	return response;
}, function(error){
	window.spin_count = 0;
	document.body.classList.remove('axios-spinner');
	return Promise.reject(error);
});


tools.axios = {
	"get": (url, data, config) => {
		return axios.post(url, data, config);
	}
};


module.exports = tools;
